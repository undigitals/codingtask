import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';

// import MainTabNavigator from './MainTabNavigator';


import HomeScreen from '../screens/HomeScreen';
// import LinksScreen from '../screens/LinksScreen';
// import SettingsScreen from '../screens/SettingsScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen'
import SignInScreen from '../screens/SignInScreen'
import OtherScreen from '../screens/OtherScreen'

export const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen });
export const AuthStack = createStackNavigator({ SignIn: SignInScreen });


export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    // Main: MainTabNavigator,
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
}
  )
);
